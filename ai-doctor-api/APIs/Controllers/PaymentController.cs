﻿using AppServices.Models;
using AppServices.Services.Intefaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/payment")]
    [ApiController]
    [AllowAnonymous]

    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;

        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Create([FromBody]CreatePaymentModel createPaymentModel)
        {
            var result = await _paymentService.CreatePayment(createPaymentModel);
            return Ok(result);
        }
    }
}
