﻿using AppServices.Models;
using AppServices.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AuthorizeController : Controller
    {
        private readonly IAuthorizeServices _authorizeServices;
        public AuthorizeController(IAuthorizeServices authorizeServices)
        {
            _authorizeServices = authorizeServices;
        }
        [HttpGet("user")]
        //[Authorize(Policy = IdentityData.Member)]
        public async Task<IActionResult> GetUser()
        {
            var result = await _authorizeServices.GetUser();
            return Ok(result);
        }
        [HttpGet("doctor/{id}")]
        //[Authorize(Policy = IdentityData.Member)]
        public async Task<IActionResult> GetDoctorDetails(string id)
        {
            var result = await _authorizeServices.GetDoctor(id);
            return Ok(result);
        }
        [HttpGet("doctors")]
        //[Authorize(Policy = IdentityData.Member)]
        public async Task<IActionResult> GetAllDoctors(int pageIndex = 1, int pageSize = 10)
        {
            var result = await _authorizeServices.GetAllDoctor(pageIndex, pageSize);
            return Ok(result);
        }
        [AllowAnonymous]
        [HttpPost("sign-up")]
        public async Task<IActionResult> Register([FromBody] RegisterUserModel user)
        {
            var result = await _authorizeServices.CreateUser(user);
            return Ok(result);
        }
        [AllowAnonymous]
        [HttpPost("sign-in")]
        public async Task<IActionResult> Login([FromBody] LoginModel user)
        {
            var result = await _authorizeServices.VerifyUser(user);
            return Ok(result);
        }
        [HttpPut]
        //[Authorize(Policy = IdentityData.Member)]
        public async Task<IActionResult> UpdateInfo([FromQuery] string id, [FromBody] RegisterUserModel user)
        {
            var result = await _authorizeServices.UpdateUser(id, user);
            return Ok(result);
        }
    }
}
