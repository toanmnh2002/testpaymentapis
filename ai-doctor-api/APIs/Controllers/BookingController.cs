﻿using AppServices.Models;
using AppServices.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    
    public class BookingController : Controller
    {
        private readonly IBookingServices _bookingServices;

        public BookingController(IBookingServices bookingServices)
        {
            _bookingServices = bookingServices;
        }
        [Authorize(Policy = IdentityData.Member)]
        [HttpGet("{doctorId}/schedules")]
        public async Task<IActionResult> GetBookings(string doctorId)
        {
            var result = await _bookingServices.GetAllBooking(doctorId);
            return Ok(result);
        }
        [Authorize(Policy = IdentityData.Patient)]
        [HttpPost("{bookingId}/{patientId}/feedback/{doctorId}")]
        public async Task<IActionResult> Rate(string bookingId, string patientId, string doctorId, [FromBody] RateModel rate)
        {
            var result = await _bookingServices.Rating(patientId, doctorId, bookingId, rate.Comment, rate.RateNum);
            return Ok(result);
        }
        [Authorize(Policy = IdentityData.Doctor)]
        [HttpPut]
        public async Task<IActionResult> UpdateBooking([FromQuery] string id, [FromBody] UpdateBookingModel model)
        {
            var result = await _bookingServices.UpdateBooking(id, model);
            return Ok(result);
        }
        [Authorize(Policy = IdentityData.Doctor)]
        [HttpPost]
        public async Task<IActionResult> CreateBooking([FromBody] CreateBookingModel model)
        {
            var result = await _bookingServices.CreateBooking(model);
            return Ok(result);
        }
        [Authorize(Policy = IdentityData.Patient)]
        [HttpPost("book")]
        public async Task<IActionResult> Booking([FromQuery] string bookingId, [FromBody] BookingRequestModel model)
        {
            var result = await _bookingServices.Booking(bookingId, model);
            return Ok(result);
        }
        [Authorize(Policy = IdentityData.Patient)]
        [HttpPut("cancel/{id}")]
        public async Task<IActionResult> CancelBooking(string id)
        {
            var result = await _bookingServices.CancelBooking(id);
            return Ok(result);
        }
        [Authorize(Policy = IdentityData.Doctor)]
        [HttpGet("{id}")]
        public async Task<IActionResult> BookingDetails(string id)
        {
            var result = await _bookingServices.GetBookingDetail(id);
            return Ok(result);
        }
    }
}
