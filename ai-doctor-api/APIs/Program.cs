using APIs;
using APIs.Middlewares;
using AppServices;
using AppServices.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using MongoDB.Driver;
using System.Numerics;

var builder = WebApplication.CreateBuilder(args);

#region AppSettings
//var appSettings = builder.Configuration.GetSection("AppConfig").Get<AppConfig>();
//builder.Services.AddSingleton(appSettings);

var mongoSettings = builder.Configuration.GetSection("MongoDBSettings").Get<MongoDBSettings>();
builder.Services.AddSingleton(u =>
{
    var mongoDb = (new MongoClient(mongoSettings?.ConnectionURI)).GetDatabase(mongoSettings?.DatabaseName);
    return mongoDb;
});

var momoConfig = builder.Configuration.GetSection("MomoConfig").Get<MomoConfig>();
builder.Services.AddSingleton(momoConfig);

var jwtSection = builder.Configuration.GetSection("JWTSection").Get<JWTSection>();
builder.Services.AddSingleton(jwtSection);
#endregion

#region Services
builder.Services.BuildServices(builder.Configuration);
builder.Services.CoreServices();
// add cors
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
    {
        policy.WithOrigins("http://localhost:3000")
              .AllowAnyHeader()
              .AllowAnyMethod();
    });
});
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options => builder.Configuration.Bind("JWTSection", options));

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy(IdentityData.Member, policy => policy.RequireRole("Doctor", "Patient", "Hospital", "System"));
    options.AddPolicy(IdentityData.Doctor, policy => policy.RequireRole("Doctor", "System"));
    options.AddPolicy(IdentityData.Hospital, policy => policy.RequireRole("Hospital", "System"));
    options.AddPolicy(IdentityData.Patient, policy => policy.RequireRole("Patient"));
});
#endregion

#region App
var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors();

app.UseMiddleware<JwtMiddleware>();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
#endregion