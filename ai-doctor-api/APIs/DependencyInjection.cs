﻿using APIs.Services;
using AppServices.Services;

namespace APIs
{
    public static class DependencyInjection
    {
        public static IServiceCollection BuildServices(this IServiceCollection services, IConfiguration config)
        {
            #region CoreServices
            services.AddScoped<IClaimsServices, ClaimsServices>();
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddHttpContextAccessor();
            #endregion

            return services;
        }
    }
}
