﻿using AppServices.Entity;
using MongoDB.Driver;

namespace AppServices
{
    public class MongoDbContext
    {
        public MongoDbContext(IMongoDatabase database)
        {
            User = database.GetCollection<User>(nameof(User));
            Rate = database.GetCollection<Rate>(nameof(Rate));
            Booking = database.GetCollection<Booking>(nameof(Booking));
            PaymentMethod = database.GetCollection<PaymentMethod>(nameof(Payment));
            EnsureDB();
        }
        public IMongoCollection<User> User { get; set; }
        public IMongoCollection<Rate> Rate { get; set; }
        public IMongoCollection<Booking> Booking { get; set; }
        public IMongoCollection<PaymentMethod> PaymentMethod { get; set; }


        private void EnsureDB()
        {
            User.Indexes.CreateOne(
                new CreateIndexModel<User>(Builders<User>.IndexKeys.Descending(model => model.UserName),
                new CreateIndexOptions { Unique = true }));
        }
    }
}
