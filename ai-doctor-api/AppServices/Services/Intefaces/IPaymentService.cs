﻿using AppServices.Models;
using AppServices.Payment.Momo.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppServices.Services.Intefaces
{
    public interface IPaymentService
    {
        Task<ResponseHelper<CreatePaymentResponse>> CreatePayment(CreatePaymentModel request);
    }
}
