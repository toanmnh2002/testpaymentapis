﻿namespace AppServices.Services
{
    public interface IClaimsServices
    {
        string? GetCurrentUser();
    }
}
