﻿using AppServices.Entity;
using AppServices.Models;
using AutoMapper;

namespace AppServices.Services
{
    public class MapperServices : Profile
    {
        public MapperServices()
        {
            CreateMap<User, UserModel>()
                     .ForMember(des => des.Role, src => src.MapFrom(x => x.Role.ToString()))
                     .ReverseMap();
            CreateMap<User, RegisterUserModel>()
                .ForMember(des => des.Role, src => src.MapFrom(x => (int)x.Role))
                .ReverseMap();
            CreateMap<User, LoginModel>().ReverseMap();
            CreateMap<Rate, RateModel>().ReverseMap();
            CreateMap<Booking, CreateBookingModel>().ReverseMap();
            CreateMap<Booking, UpdateBookingModel>().ReverseMap();
            CreateMap<Booking, BookingDetailsModel>().ReverseMap();
            CreateMap<BookingInfor, BookingInforModel>().ReverseMap();
            CreateMap<Booking, BookingModel>()
                     .ForMember(des => des.BookingStatus, src => src.MapFrom(x => x.BookingStatus.ToString()))
                     .ForMember(des => des.PaidStatus, src => src.MapFrom(x => x.PaidStatus.ToString()))
                     .ReverseMap();
            CreateMap<PaymentMethod, CreatePaymentModel>().ReverseMap();
        }
    }
}
