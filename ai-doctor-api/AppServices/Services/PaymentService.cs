﻿using AppServices.Entity;
using AppServices.Models;
using AppServices.Payment.Momo.Request;
using AppServices.Payment.Momo.Response;
using AppServices.Services.Intefaces;
using AutoMapper;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppServices.Services
{

    public class PaymentService : IPaymentService
    {
        private readonly MongoDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IClaimsServices _claimsServices;
        private readonly MomoConfig _momoConfig;

        public PaymentService(MongoDbContext dbContext, IMapper mapper, IClaimsServices claimsServices, MomoConfig momoConfig)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _claimsServices = claimsServices;
            _momoConfig = momoConfig;
        }
        public async Task<ResponseHelper<CreatePaymentResponse>> CreatePayment(CreatePaymentModel request)
        {
            try
            {
                //Add payment method
                //var currentUser = _claimsServices.GetCurrentUser();
                //var user = await _dbContext.User.Find(x => x.Id == currentUser).FirstOrDefaultAsync();
                //if (currentUser == null || user == null || user.Role != Role.Patient)
                //{
                //    return new ResponseHelper<CreatePaymentResponse> { Errors = "We can't find you in my system." };
                //}
                var obj = _mapper.Map<PaymentMethod>(request);
                obj.PaymentName = "Momo";
                await _dbContext.PaymentMethod.InsertOneAsync(obj);
                //Momo payment
                var paymentUrl = string.Empty;
                var momoRequest = new MomoRequest(
                    _momoConfig.PartnerCode, "c4a88482-3885-4cdb-8b4a-ace8eac16fcd" ?? string.Empty, (long)request.RequiredAmount!,
                     "96806732-b972-4049-8dc0-73a337f868f2" ?? string.Empty,
                    request.PaymentContent ?? string.Empty,
                    _momoConfig.ReturnUrl, _momoConfig.IpnUrl,
                    "captureWallet", string.Empty);
                momoRequest.MakeSignature(_momoConfig.AccessKey, _momoConfig.SecretKey);
                (bool createMomoLinkResult, string? createMessage) = momoRequest.GetLink(_momoConfig.PaymentUrl);
                if (createMomoLinkResult)
                {
                    paymentUrl = createMessage;
                }
                var result = new CreatePaymentResponse
                {
                    PaymentId = obj.Id ?? string.Empty,
                    PaymentUrl = paymentUrl,
                };
                return new ResponseHelper<CreatePaymentResponse>
                {
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new ResponseHelper<CreatePaymentResponse>
                {
                    Errors = ex.Message
                };
            }
        }
    }
}
