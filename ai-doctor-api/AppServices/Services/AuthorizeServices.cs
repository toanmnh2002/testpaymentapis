﻿using AppServices.Entity;
using AppServices.Models;
using AutoMapper;
using MongoDB.Driver;

namespace AppServices.Services
{
    public class AuthorizeServices : IAuthorizeServices
    {
        private readonly JWTSection _jwtSection;
        private readonly MongoDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IClaimsServices _claimsServices;
        public AuthorizeServices(
            MongoDbContext dbContext,
            IMapper mapper,
            JWTSection jwtSection,
            IClaimsServices claimsServices)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _jwtSection = jwtSection;
            _claimsServices = claimsServices;

        }

        public async Task<ResponseHelper<RegisterUserModel>> CreateUser(RegisterUserModel user)
        {
            try
            {
                var obj = _mapper.Map<User>(user);
                obj.Satl = Utils.Satl();
                obj.Password = Utils.Hash(user.Password, obj.Satl);
                await _dbContext.User.InsertOneAsync(obj);
                return new ResponseHelper<RegisterUserModel>
                {
                    Data = user
                };
            }
            catch (Exception ex)
            {
                return new ResponseHelper<RegisterUserModel>
                {
                    Data = null,
                    Errors = ex.Message
                };
            }
        }

        public async Task<ResponseHelper<List<UserModel>?>> GetAllDoctor(int pageIndex = 1, int pageSize = 10)
        {
            var result = await _dbContext.User.Find(x => x.Role == Role.Doctor)
                                              .SortByDescending(x => x.Rate)
                                              .Skip((pageIndex - 1) * pageSize)
                                              .Limit(pageSize)
                                              .ToListAsync();
            return new ResponseHelper<List<UserModel>?>
            {
                Data = _mapper.Map<List<UserModel>?>(result)
            };
        }

        public async Task<ResponseHelper<UserModel>> GetDoctor(string doctorId)
        {
            var result = await _dbContext.User.Find(x => x.Id == doctorId).FirstOrDefaultAsync();
            return new ResponseHelper<UserModel>
            {
                Data = _mapper.Map<UserModel>(result)
            };
        }

        public async Task<ResponseHelper<UserModel>> GetUser()
        {
            var currentUser = _claimsServices.GetCurrentUser();
            var user = await _dbContext.User.Find(x => x.Id == currentUser).FirstOrDefaultAsync();
            if (currentUser == null || user == null)
            {
                return new ResponseHelper<UserModel> { Data = null, Errors = "Login fail." };
            }
            return new ResponseHelper<UserModel>
            {
                Data = _mapper.Map<UserModel>(user)
            };
        }

        public async Task<ResponseHelper<UserModel>> UpdateUser(string id, RegisterUserModel userModel)
        {
            var user = await _dbContext.User.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (user == null)
            {
                return new ResponseHelper<UserModel> { Data = null, Errors = "I can't find you in my system." };
            }
            await BuildUpdate(user.Id, userModel);
            return new ResponseHelper<UserModel>
            {
                Data = _mapper.Map<UserModel>(user)
            };
        }

        public async Task<ResponseHelper<string>> VerifyUser(LoginModel user)
        {
            var response = new ResponseHelper<string>();
            var obj = await _dbContext.User.Find(x => x.UserName == user.UserName).FirstOrDefaultAsync();
            if (obj == null)
            {
                response.Data = null;
                response.Errors = "I can't find you in my system.";
                return response;
            }
            var isVerify = Utils.Verify(user.Password, obj.Password);
            if (!isVerify)
            {
                response.Data = null;
                response.Errors = "Please re-check your user name or password.";
            }
            var role = GetRoleString(obj.Role);
            response.Data = Utils.GenerateJwtToken(obj.Id, user.UserName, role, _jwtSection);

            return response;
        }

        #region private-methods
        private async Task BuildUpdate(string id, RegisterUserModel model)
        {
            var salt = Utils.Satl();
            var password = Utils.Hash(model.Password, salt);
            var update = Builders<User>.Update
                                       .Set(x => x.Email, model.Email)
                                       .Set(x => x.Password, password)
                                       .Set(x => x.Satl, salt)
                                       .Set(x => x.Address, model.Address)
                                       .Set(x => x.Specialist, model.Specialist)
                                       .Set(x => x.YearOfExp, model.YearOfExp)
                                       .Set(x => x.Phone, model.Phone)
                                       .Set(x => x.FullName, model.FullName)
                                       .Set(x => x.CertificationImgs, model.CertificationImgs);
            await _dbContext.User.UpdateOneAsync(x => x.Id == id, update);
        }
        private string GetRoleString(Role roleEnum)
        {
            switch (roleEnum)
            {
                case Role.Patient:
                    return "Patient";
                case Role.System:
                    return "System";
                case Role.Hospital:
                    return "Hospital";
                case Role.Doctor:
                    return "Doctor";
                default:
                    return "Unknown";
            }
        }
        #endregion
    }
}
