﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace AppServices.Entity
{
    public class Booking
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string DoctorId { get; set; }
        public string? PatientId { get; set; }
        public DateTime Date { get; set; }
        public BookingStatus BookingStatus { get; set; } = BookingStatus.None;
        public PaidStatus PaidStatus { get; set; } = PaidStatus.None;
        public decimal Price { get; set; }
        public string Address { get; set; }
        public BookingInfor? BookingInfor { get; set; }
    }
    public class BookingInfor
    {
        public string PatientName { get; set; }
        public string Note { get; set; }
        public string PatientPhone { get; set; }
    }
}
