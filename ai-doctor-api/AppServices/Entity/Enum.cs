﻿namespace AppServices.Entity
{
    public enum Role
    {
        System = 0,
        Patient = 1,
        Doctor = 2,
        Hospital = 3
    }
    public enum BookingStatus
    {
        None = 0,
        Booked = 1,
        Cancel = 3
    }
    public enum PaidStatus
    {
        None = 0,
        Paid = 1,
        Refund = 2
    }
}
