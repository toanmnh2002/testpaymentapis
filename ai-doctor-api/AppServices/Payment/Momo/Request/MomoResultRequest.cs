﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppServices.Payment.Momo.Request
{
    [BindProperties]
    public class MomoResultRequest
    {
        public string? partnerCode { get; set; } 
        public string? orderId { get; set; } 
        public string? requestId { get; set; } 
        public long amount { get; set; }
        public string? orderInfo { get; set; } 
        public string? orderType { get; set; } 
        public string? transId { get; set; } 
        public string? message { get; set; } 
        public int resultCode { get; set; }
        public string? payType { get; set; } 
        public long responseTime { get; set; }
        public string? extraData { get; set; } 
        public string? signature { get; set; } 

        public bool IsValidSignature(string accessKey, string secretKey)
        {
            var rawHash = "accessKey=" + accessKey +
                   "&amount=" + this.amount +
                   "&extraData=" + this.extraData +
                   "&message=" + this.message +
                   "&orderId=" + this.orderId +
                   "&orderInfo=" + this.orderInfo +
                   "&orderType=" + this.orderType +
                   "&partnerCode=" + this.partnerCode +
                   "&payType=" + this.payType +
                   "&requestId=" + this.requestId +
                   "&responseTime=" + this.responseTime +
                   "&resultCode=" + this.resultCode +
                   "&transId=" + this.transId;
            var checkSignature = HashHelper.HmacSHA256(rawHash, secretKey);
            return this.signature.Equals(checkSignature);
        }
    }
}
