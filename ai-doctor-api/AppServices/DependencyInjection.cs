﻿using AppServices.Services;
using AppServices.Services.Intefaces;
using Microsoft.Extensions.DependencyInjection;

namespace AppServices
{
    public static class DependencyInjection
    {
        public static IServiceCollection CoreServices(this IServiceCollection services)
        {
            #region Services
            services.AddScoped<MongoDbContext>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IAuthorizeServices, AuthorizeServices>();
            services.AddScoped<IBookingServices, BookingServices>();
            services.AddScoped<IPaymentService, PaymentService>();
            #endregion

            return services;
        }
    }
}
