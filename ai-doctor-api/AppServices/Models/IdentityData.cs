﻿public class IdentityData
{
    public const string Doctor = "Doctor";
    public const string Member = "Member";
    public const string Hospital = "Hospital";
    public const string Patient = "Patient";
    public const string Role = "role";
}