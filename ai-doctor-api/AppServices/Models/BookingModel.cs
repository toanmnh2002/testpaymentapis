﻿using AppServices.Entity;

namespace AppServices.Models
{
    public class BookingModel
    {
        public string? Id { get; set; }
        public string DoctorId { get; set; }
        public string? PatientId { get; set; }
        public DateTime Date { get; set; }
        public string? BookingStatus { get; set; }
        public string? PaidStatus { get; set; }
        public decimal Price { get; set; }
        public string Address { get; set; }
    }
    public class UpdateBookingModel
    {
        public string? Id { get; set; }
        public string DoctorId { get; set; }
        public string? PatientId { get; set; }
        public DateTime Date { get; set; }
        public BookingStatus? BookingStatus { get; set; }
        public PaidStatus? PaidStatus { get; set; }
        public decimal Price { get; set; }
        public string Address { get; set; }
    }
    public class CreateBookingModel
    {
        public string DoctorId { get; set; }
        public string? PatientId { get; set; }
        public DateTime Date { get; set; }
        public BookingStatus? BookingStatus { get; set; }
        public PaidStatus? PaidStatus { get; set; }
        public decimal Price { get; set; }
        public string Address { get; set; }
    }
    public class BookingDetailsModel
    {
        public string DoctorId { get; set; }
        public string? PatientId { get; set; }
        public DateTime Date { get; set; }
        public BookingStatus? BookingStatus { get; set; }
        public PaidStatus? PaidStatus { get; set; }
        public decimal Price { get; set; }
        public string Address { get; set; }
        public BookingInforModel? BookingInfor { get; set; }
    }
    public class BookingInforModel
    {
        public string PatientName { get; set; }
        public string Note { get; set; }
        public string PatientPhone { get; set; }
    }
    public class BookingRequestModel
    {
        public string DoctorId { get; set; }
        public string? PatientId { get; set; }
        public string PatientName { get; set; }
        public string Note { get; set; }
        public string PatientPhone { get; set; }
    }
}
