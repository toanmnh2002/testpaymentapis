﻿namespace AppServices.Models
{
    public class RegisterUserModel
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Role { get; set; }
        public string? Address { get; set; }
        public string? Specialist { get; set; }
        public double? YearOfExp { get; set; }
        public List<string>? CertificationImgs { get; set; }
    }
}
