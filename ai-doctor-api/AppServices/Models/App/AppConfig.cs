﻿namespace AppServices.Models
{
    public class AppConfig
    {
        
    }
    public class MongoDBSettings
    {
        public string ConnectionURI { get; set; } = null!;
        public string DatabaseName { get; set; } = null!;
    }
    public class JWTSection
    {
        public string SecretKey { get; set; }
        public int ExpiresInDays { get; set; }
    }
    public class MomoConfig
    {
        public string? PartnerCode { get; set; }
        public string? ReturnUrl { get; set; }
        public string? IpnUrl { get; set; }
        public string? AccessKey { get; set; }
        public string? SecretKey { get; set; }
        public string? PaymentUrl { get; set; }
    }
}
